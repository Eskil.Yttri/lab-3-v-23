package no.uib.inf101.terminal;

// UiB INF101 ShellLab - Main.java
// Dette er filen som inneholder main-metoden.

public class Main {

  public static void main(String[] args) {
    // Create a new shell
    SimpleShell shell = new SimpleShell();
    shell.installCommand(new CmdEcho());
    // When you type hello into terminal, output will be Hello World! 
    shell.installCommand(new CmdJavaProgram("hello", HelloWorld.class));
    // WHen you type new, a new terminal will open.
    shell.installCommand(new CmdJavaProgram("new", Main.class));


    // Run the shell in the terminal GUI
    Terminal gui = new Terminal(shell);
    gui.run();

  
  }
}