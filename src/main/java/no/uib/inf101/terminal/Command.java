package no.uib.inf101.terminal;

public interface Command {
    /**
     * Called when a command is run
     * @param args 
     * @return
     */

    String run(String[] args);

    /**
     * 
     * @return name of the command
     */

    String getName();
}


